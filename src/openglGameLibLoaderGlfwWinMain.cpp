#include "common.cpp"
#include "soa.hpp"
#include "Str.cpp"
#include "StrBuf.cpp"
#include "print.cpp"
#include "args.cpp"
#include "utf8.cpp"
#include "vector.hpp"
#include "List.hpp"
#include "Grid.hpp"
#include "Hash.cpp"
#include "map.hpp"
#include "ScopeExit.hpp"
#include "file.cpp"
#include "allocator.hpp"
#include "HeapAllocator.cpp"
#include "PageAllocator.cpp"
#include "gl3w_Functions.cpp"
#include "gl3w_Library.cpp"
#include "assetManager_config_assetType.cpp"
#include "openglGame_common.cpp"
#include "openglGame_AssetLabel.hpp"
#include "magicaVoxel_Palette.cpp"
#include "magicaVoxel_File.cpp"
#include "magicaVoxel_VoxelGrid.cpp"
#include "magicaVoxel_Mesh.hpp"
#include "magicaVoxel_loadMesh.cpp"
#include <cstdlib>
#include <cstdio>
#include <malloc.h>
#include <GLFW/glfw3.h>
#include "filesystem.cpp"
#include "glfwCallbacks.cpp"
#include "openglGame_updateAsset.cpp"



namespace GameStatus
{
  enum Enum
  {
    NOT_INITIALISED = 0,
    INITIALISED,
    FAILED_INITIALISATION
  };
}

struct GameLib
{
  HMODULE                          dll;
  ZStr                             pathCopiedTo;

  openglGame::LibInitFunction     *libInit;
  openglGame::LibShutdownFunction *libShutdown;
  openglGame::GameInitFunction    *gameInit;
  openglGame::GameUpdateFunction  *gameUpdate;

  openglGame::AssetList           *assetList;

  GameStatus::Enum                 status;

  bool isLoaded()
  {
    return dll && libInit && libShutdown && gameInit && gameUpdate && assetList;
  }
};

struct GameAssets
{
  union
  {
    void *soaIndex[3];
    struct
    {
      void                          **assets;
      openglGame::AssetStatus::Enum  *statuses;
      u64                            *fileTimes;
    };
  };

  static size_t sizeWith(u64 count)
  {
    return soa::bufSizeWith<void *, openglGame::AssetStatus::Enum, u64>(count);
  }

  void initWithBuf(void *buf, u64 count)
  {
    soa::initIndex<void *, openglGame::AssetStatus::Enum, u64>(soaIndex, buf, count);
  }
};

u64 loadGameLib(GameLib *lib, ZStr game_dll_full_path, ZStr temp_dll_full_path)
{
  assert(!lib->dll);
  *lib = {};

  u64 file_last_write_time = 0;

  if (!filesystem::exists(game_dll_full_path))
  {
    println(stdout, "No such file: "_s, game_dll_full_path);
  }
  else
  {
    file_last_write_time = filesystem::lastWriteTime(game_dll_full_path);

    CopyFileA(game_dll_full_path, temp_dll_full_path, FALSE);
    lib->pathCopiedTo = temp_dll_full_path;

    lib->dll = LoadLibraryA(temp_dll_full_path);
    if(lib->dll)
    {
      Str loader_openglGame_common_pkg_ver = STR(OPENGLGAME_COMMON_PKG_VER);
      Str lib_openglGame_common_pkg_ver = *((Str *) GetProcAddress(lib->dll, ZSTR(openglGame_COMMON_PKG_VER)));
      if (loader_openglGame_common_pkg_ver != lib_openglGame_common_pkg_ver)
      {
        print(stdout,
          "Loaded game DLL uses unexpected openglGame common package version:\n  "_s,
          lib_openglGame_common_pkg_ver, " (openglGame_libLoader_app uses "_s,
          loader_openglGame_common_pkg_ver, ")\n"_s
        );
      }

      print(stdout, "Loaded game DLL: "_s, temp_dll_full_path, "\n"_s);

      lib->libInit     = (openglGame::LibInitFunction     *)GetProcAddress(lib->dll, ZSTR(openglGame_LIB_INIT));
      lib->libShutdown = (openglGame::LibShutdownFunction *)GetProcAddress(lib->dll, ZSTR(openglGame_LIB_SHUTDOWN));
      lib->gameInit    = (openglGame::GameInitFunction    *)GetProcAddress(lib->dll, ZSTR(openglGame_GAME_INIT));
      lib->gameUpdate  = (openglGame::GameUpdateFunction  *)GetProcAddress(lib->dll, ZSTR(openglGame_GAME_UPDATE));
      lib->assetList   = (openglGame::AssetList           *)GetProcAddress(lib->dll, ZSTR(openglGame_ASSET_LIST));

      println(stdout, "  Library Init Function:     "_s, lib->libInit     ? "OK"_s : "NOT FOUND"_s);
      println(stdout, "  Library Shutdown Function: "_s, lib->libShutdown ? "OK"_s : "NOT FOUND"_s);
      println(stdout, "  Game Init Function:        "_s, lib->gameInit    ? "OK"_s : "NOT FOUND"_s);
      println(stdout, "  Game Update Function:      "_s, lib->gameUpdate  ? "OK"_s : "NOT FOUND"_s);
      println(stdout, "  Asset List:                "_s, lib->assetList   ? "OK"_s : "NOT FOUND"_s);

      if (lib_openglGame_common_pkg_ver.endsWith("-dirty"_s))
      {
        println(stdout, "WARNING - Using openglGame common pkg ver: "_s, lib_openglGame_common_pkg_ver);
      }
    }
    else
    {
      print(stdout, "Failed to load DLL: "_s, game_dll_full_path, "\n"_s);
    }
  }

  return file_last_write_time;
}

void unloadGameLib(GameLib *lib)
{
  if(lib->dll)
  {
    FreeLibrary(lib->dll);

    *lib = {};
  }
}

Str getGlDebugMessageTypeName(GLenum type)
{
  switch (type)
  {
    case GL_DEBUG_TYPE_ERROR:               return "Error"_s;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: return "Deprecated Behavior"_s;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  return "Undefined Behavior"_s;
    case GL_DEBUG_TYPE_PORTABILITY:         return "Portability"_s;
    case GL_DEBUG_TYPE_PERFORMANCE:         return "Performance"_s;
    case GL_DEBUG_TYPE_MARKER:              return "Marker"_s;
    case GL_DEBUG_TYPE_PUSH_GROUP:          return "Push Group"_s;
    case GL_DEBUG_TYPE_POP_GROUP:           return "Pop Group"_s;
    case GL_DEBUG_TYPE_OTHER:               return "Other"_s;
    default:                                return "???"_s;
  }
}

Str getGlDebugMessageSeverityName(GLenum severity)
{
  switch (severity)
  {
    case GL_DEBUG_SEVERITY_LOW:          return "Low"_s;
    case GL_DEBUG_SEVERITY_MEDIUM:       return "Medium"_s;
    case GL_DEBUG_SEVERITY_HIGH:         return "High"_s;
    case GL_DEBUG_SEVERITY_NOTIFICATION: return "Notification"_s;
    default:                             return "???"_s;
  }
}

void APIENTRY handleDebugMessage(GLenum source, GLenum type, GLuint id,
                                 GLenum severity, GLsizei length,
                                 const GLchar *message, const void *userParam)
{
  if (severity == GL_DEBUG_SEVERITY_MEDIUM || severity == GL_DEBUG_SEVERITY_HIGH)
  {
    println(stderr,
      "GL ("_s, getGlDebugMessageTypeName(type), " / "_s, fmt(id), " / "_s,
        getGlDebugMessageSeverityName(severity), "): "_s, Str{(u64)length, message}
    );
  }
}

int main(u32 argc, char **argv)
{
  ZStr  game_dll_path    = ZStr::empty();

  f64  scale_multiplier  = 1.0;
  bool scale_fudge_set   = false;

  args::Array *args;
  args_Array_localAllocAndInit(args, argc, argv);
  args::Iterator arg_it = args->start();

  // Argument 0 is always the executable path
  assert(arg_it.valid());
  Str executable_path = arg_it.getArg();
  arg_it.step();

  {
    Str  game_dll_path_arg     = Str::empty();
    bool game_dll_path_arg_set = false;

    // First non-option argument is the game dll path.
    // Options before game dll path are for the loader.
    // Arguments after game dll path are for the game.
    while (arg_it.valid() && !game_dll_path_arg_set)
    {
      if (arg_it.getType() == args::Type::NON_OPT)
      {
        game_dll_path_arg     = arg_it.getArg();
        game_dll_path_arg_set = true;
      }
      else if (arg_it.matchOpt("scale-fudge"_s))
      {
        if (scale_fudge_set)
        {
          println(stdout, "Warning: Option \"scale-fudge\" re-specified"_s);
        }

        Str param_str = arg_it.getParam();

        if (!param_str.length)
        {
          println(stdout, "Warning: Option \"scale-fudge\" requires a parameter"_s);
        }
        else
        {
          f64 param = param_str.parseF64();

          if (isnan(param) || param <= 0.0)
          {
            println(stdout, "Warning: Invalid \"scale-fudge\" parameter: \""_s, param_str, "\""_s);
          }
          else
          {
            scale_multiplier = 1.0 / param;
            scale_fudge_set  = true;
          }
        }
      }
      else
      {
        println(stdout, "Warning: Ignoring unexpected option: \""_s, arg_it.getArg(), "\""_s);
      }

      arg_it.step();
    }

    if (game_dll_path_arg_set)
    {
      StrBuf::Variable *buf;
      sprintLocal(buf, game_dll_path_arg);
      game_dll_path = buf->zStr();
    }
    else
    {
      println(stdout, "No openglGame DLL specified, exiting."_s);
      return 1;
    }
  }

  ZStr game_dll_temp_path_a;
  {
    StrBuf::Variable *buf;
    sprintLocal(buf, game_dll_path, ".a.tmp"_s);
    game_dll_temp_path_a = buf->zStr();
  }

  ZStr game_dll_temp_path_b;
  {
    StrBuf::Variable *buf;
    sprintLocal(buf, game_dll_path, ".b.tmp"_s);
    game_dll_temp_path_b = buf->zStr();
  }

  ZStr game_dll_lock_path;
  {
    StrBuf::Variable *buf;
    sprintLocal(buf, game_dll_path, ".lock"_s);
    game_dll_lock_path = buf->zStr();
  }

  println(stdout,
    "Using DLL \""_s, game_dll_path,
    "\", copied to \""_s, game_dll_temp_path_a,
    "\" or \""_s, game_dll_temp_path_b, "\""_s
  );

  if (!glfwInit())
  {
    print(stdout, "Error: glfwInit() failed\n"_s);
    return 1;
  }

  fprintf(stdout, "GLFW %s\n", glfwGetVersionString());

  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_SRGB_CAPABLE, GL_TRUE);

  // FIXME: ifdef debug
  glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);

  GLFWwindow *window = glfwCreateWindow(640, 480, "OpenGL Game Library Loader", NULL, NULL);
  if (!window)
  {
    print(stdout, "Error: glfwCreateWindow() failed\n"_s);
    return 1;
  }

  glfwCallbacks::attach(window);
  glfwMakeContextCurrent(window);

  glfwSwapInterval(1);

  openglGame::GameResources resources = {};

  resources.platformHeap = STD_HEAP;

  gl3w::Library gl3w_library = {};
  gl3w_library.init();
  if (!gl3w_library.getFunctions(&resources.gl))
  {
    printf("Failed to initiailze gl3w\n");
    return 1;
  }

  union
  {
    struct
    {
      bool KHR_debug;
    };
    bool array[1];
  } gl_exts;

  {
    Str gl_ext_names[1] = {
      "GL_KHR_debug"_s
    };
    resources.gl.checkExtensions(gl_ext_names, gl_exts.array, arrayCount(gl_exts.array));
  }

  if (gl_exts.KHR_debug)
  {
    println(stdout, "Extension KHR_debug found, enabling gl debug output"_s);
    resources.gl.DebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, 0, GL_TRUE);
    resources.gl.DebugMessageCallback(&handleDebugMessage, 0);
    resources.gl.Enable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  }
  else
  {
    println(stdout, "Extension KHR_debug not found, cannot enable gl debug output"_s);
  }

  // FIXME: ifdef debug
  resources.debugResources.clipboard.copy  = glfwCallbacks::copy;
  resources.debugResources.clipboard.paste = glfwCallbacks::paste;

  printf("OpenGL %s, GLSL %s\n",
         resources.gl.GetString(GL_VERSION),
         resources.gl.GetString(GL_SHADING_LANGUAGE_VERSION));

  GameLib game_lib = {};
  GameStatus::Enum game_status = GameStatus::NOT_INITIALISED;

  u64 game_dll_last_write_time = loadGameLib(&game_lib, game_dll_path, game_dll_temp_path_a);

  GameAssets game_assets;
  void *game_assets_buf;
  if (game_lib.isLoaded())
  {
    game_assets_buf = malloc(GameAssets::sizeWith(game_lib.assetList->count));

    game_assets.initWithBuf(
      game_assets_buf,
      game_lib.assetList->count
    );

    for (u64 i = 0; i < game_lib.assetList->count; ++i)
    {
      game_assets.assets[i] = 0;
      game_assets.statuses[i] = openglGame::AssetStatus::UNLOADED;
      game_assets.fileTimes[i] = 0;
    }
  }
  else
  {
    game_assets = {};
    game_assets_buf = 0;
  }

  while (!glfwWindowShouldClose(window))
  {
    if (!filesystem::exists(game_dll_lock_path))
    {
      u64 dll_file_time = filesystem::lastWriteTime(game_dll_path);
      if (game_dll_last_write_time != dll_file_time)
      {
        GameLib new_game_lib = {};
        game_dll_last_write_time = loadGameLib(&new_game_lib, game_dll_path,
                                               game_lib.pathCopiedTo == game_dll_temp_path_a ?
                                                 game_dll_temp_path_b : game_dll_temp_path_a);

        if (!new_game_lib.isLoaded())
        {
          println(stdout, "Failed to load new game dll!"_s);
        }
        else
        {
          game_lib.libShutdown(&resources);

          GameAssets new_game_assets;
          void *new_game_assets_buf = malloc(GameAssets::sizeWith(new_game_lib.assetList->count));

          new_game_assets.initWithBuf(
            new_game_assets_buf,
            new_game_lib.assetList->count
          );

          if (game_lib.isLoaded())
          {
            for (u64 new_asset_idx = 0;
                 new_asset_idx < new_game_lib.assetList->count;
                 ++new_asset_idx)
            {
              openglGame::AssetLabel *new_asset_label = &new_game_lib.assetList->labels[new_asset_idx];

              u64 old_asset_idx = 0;
              for (;
                   old_asset_idx < game_lib.assetList->count;
                   ++old_asset_idx)
              {
                openglGame::AssetLabel *old_asset_label = &game_lib.assetList->labels[old_asset_idx];

                if (new_asset_label->type   == old_asset_label->type &&
                    new_asset_label->params == old_asset_label->params)
                {
                  break;
                }
              }

              if (old_asset_idx < game_lib.assetList->count)
              {
                new_game_assets.assets[new_asset_idx]    = game_assets.assets[old_asset_idx];
                game_assets.assets[old_asset_idx] = 0;

                new_game_assets.statuses[new_asset_idx]  = game_assets.statuses[old_asset_idx];
                game_assets.statuses[old_asset_idx] = openglGame::AssetStatus::UNLOADED;

                new_game_assets.fileTimes[new_asset_idx] = game_assets.fileTimes[old_asset_idx];
                game_assets.fileTimes[old_asset_idx] = 0;
              }
              else
              {
                new_game_assets.assets[new_asset_idx] = 0;
                new_game_assets.statuses[new_asset_idx] = openglGame::AssetStatus::UNLOADED;
                new_game_assets.fileTimes[new_asset_idx] = 0;
              }
            }

            for (u64 i = 0; i < game_lib.assetList->count; ++i)
            {
              if (openglGame::AssetStatus::isLoaded[game_assets.statuses[i]])
              {
                assert(game_assets.assets[i]);
                free(game_assets.assets[i]);
              }
              else
              {
                assert(!game_assets.assets[i]);
              }
            }
          }

          game_assets = new_game_assets;
          if (game_assets_buf) free(game_assets_buf);
          game_assets_buf = new_game_assets_buf;

          unloadGameLib(&game_lib);
          game_lib = new_game_lib;

          if (game_status == GameStatus::FAILED_INITIALISATION)
          {
            game_status = GameStatus::NOT_INITIALISED;
          }
        }
      }
    }

    bool ran_game_update = false;

    BREAKABLE_START
    {
      openglGame::Input input = {};
      glfwCallbacks::getInput(&input);

      bool reset_game = input.keyboard.keys[openglGame::KeyboardKey::ENTER].wasPressed() &&
                        ( input.keyboard.keys[openglGame::KeyboardKey::LEFT_CONTROL].isDown ||
                          input.keyboard.keys[openglGame::KeyboardKey::RIGHT_CONTROL].isDown );

      if (!game_lib.isLoaded()) break;

      for (u64 i = 0; i < game_lib.assetList->count; ++i)
      {
        openglGame::updateAsset(&game_lib.assetList->labels[i],
                               &game_assets.assets[i],
                               &game_assets.statuses[i],
                               &game_assets.fileTimes[i]);
      }

      resources.assets        = game_assets.assets;
      resources.assetStatuses = game_assets.statuses;

      if (game_lib.status == GameStatus::NOT_INITIALISED)
      {
        if (game_lib.libInit(&resources))
        {
          game_lib.status = GameStatus::INITIALISED;
        }
        else
        {
          game_lib.status = GameStatus::FAILED_INITIALISATION;
          println(stdout, "Game lib failed to initialise"_s);
        }
      }

      if (game_lib.status != GameStatus::INITIALISED) break;

      if (reset_game || game_status == GameStatus::NOT_INITIALISED)
      {
        if (game_lib.gameInit(&resources, arg_it))
        {
          game_status = GameStatus::INITIALISED;
        }
        else
        {
          game_status = GameStatus::FAILED_INITIALISATION;
          println(stdout, "Game failed to initialise"_s);
        }
      }

      if (game_status != GameStatus::INITIALISED) break;

      if (scale_multiplier != 1.0)
      {
        input.windowSize_points      *= scale_multiplier;
        input.mouse.cursorPos_points *= scale_multiplier;
      }

      game_lib.gameUpdate(&resources, &input);

      glfwSwapBuffers(window);
      ran_game_update = true;
    }
    BREAKABLE_END

    if (!ran_game_update) Sleep(100);

    glfwPollEvents();

    fflush(0);
  }

  return 0;
}
