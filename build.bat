@echo off

rem      *************
rem      * Constants *
rem      *************

set THIS_DIR=%~dp0
set SRC_DIR=%THIS_DIR%\src
set PROJECTS_ROOT_DIR=%THIS_DIR%\..\..\..
set GIT="C:\Program Files\Git\bin\git.exe"



rem      ************
rem      * Defaults *
rem      ************

set hcd_arch=amd64
set build=true
set optimise=false
set clean=false



rem      *******************
rem      * Parse Arguments *
rem      *******************

:label_parse_arguments

if [%1]==[] (
  goto label_no_further_arguments
)

set argument=%1
shift

if [%argument:~0,1%]==[/] (
  set argument=%argument:~1%
) else if [%argument:~0,2%]==[--] (
  set argument=%argument:~2%
) else (
  echo WARNING - Ignoring unexpected argument: %argument%
  goto label_parse_arguments
)


 if [%argument%]==[arch] (
  goto set_arch
) else if [%argument%]==[optimise] (
  set optimise=true
) else if [%argument%]==[clean] (
  set clean=true
) else if [%argument%]==[no-build] (
  set build=false
) else (
  echo WARNING - unknown option ignored: /%argument%
)

goto label_parse_arguments


:set_arch
shift
if [%1]==[] (
  echo WARNING: arch unspecified!
) else (
  set hcd_arch=%1
  shift
)
goto label_parse_arguments


:label_no_further_arguments



rem      *****************
rem      * Load MSVC Env *
rem      *****************

set MSVC_ENV_TOOL_DIR=%PROJECTS_ROOT_DIR%\msvcEnv\tool

call %MSVC_ENV_TOOL_DIR%\msvcEnv.bat %hcd_arch%

if %ERRORLEVEL% neq 0 (
  echo Could not load msvc env for arch: %hcd_arch%
  echo Aborting...
  exit /B 1
)



rem      ***********
rem      * Defines *
rem      ***********

set defines=

if [%optimise%]==[false] (
  set defines=%defines% -DHCD_ENABLE_SLOW_ASSERTS
)



rem      ********************
rem      * Application Main *
rem      ********************

set includes=-I %SRC_DIR%
set src_files=%SRC_DIR%\openglGameLibLoaderGlfwWinMain.cpp



rem      ************
rem      * Packages *
rem      ************

set includes=-I %THIS_DIR%\common\pkg\src ^
             -I %THIS_DIR%\gl3w\pkg\src ^
             -I %THIS_DIR%\glfw\adaptor\pkg\src ^
             -I %THIS_DIR%\assetManager\config\pkg\src ^
             -I %THIS_DIR%\magicaVoxel\pkg\src


set includes=%includes% -I %THIS_DIR%\openglGame\common\pkg\src
for /f "delims=" %%a in ('%GIT% -C %THIS_DIR%\openglGame\common\pkg describe --always --dirty') do set openglGame_common_pkg_ver=%%a
set defines=-DOPENGLGAME_COMMON_PKG_VER=\"%openglGame_common_pkg_ver%\"




rem      *************
rem      * Libraries *
rem      *************

set libraries=opengl32.lib User32.lib Gdi32.lib Shell32.lib Kernel32.lib Winmm.lib


set glfw_ver=3.1.2
set glfw_build=RelWithDebInfo
set glfw_dir=%PROJECTS_ROOT_DIR%\glfw\%glfw_ver%
set glfw_build_dir=%glfw_dir%\%hcd_arch%\%hcd_platform%\src\%glfw_build%

if not exist %glfw_build_dir% (
  echo Could not find GLFW %glfw_ver% build: %hcd_platform%-%hcd_arch% ^(%glfw_build%^)
  echo Aborting...
  exit /B 1
)

set includes=%includes% -I %glfw_dir%\include
set libraries=%libraries% %glfw_build_dir%\glfw3.lib



rem      ******************
rem      * Warnings Flags *
rem      ******************

set warning_flags=

rem treat warnings as errors
set warning_flags=%warning_flags% /WX

rem enable level 4 (almost all) warnings
set warning_flags=%warning_flags% /W4

rem disable "unreferenced formal parameter"
set warning_flags=%warning_flags% /wd4100

rem disable "conditional expression is constant"
set warning_flags=%warning_flags% /wd4127

rem allow zero-sized array in struct/union
set warning_flags=%warning_flags% /wd4200

rem allow nameless structs and unions
set warning_flags=%warning_flags% /wd4201

rem disable "conversion from 'type1' to 'type2', possible loss of data"
set warning_flags=%warning_flags% /wd4244

rem disable "declaration of argument hides class member"
set warning_flags=%warning_flags% /wd4458

rem disable "unreferenced local function has been removed"
set warning_flags=%warning_flags% /wd4505

rem disable "termination on exception is not guaranteed"
set warning_flags=%warning_flags% /wd4577

rem disable "'constexpr' does not imply 'const'"
set warning_flags=%warning_flags% /wd4814

rem disable security warnings
set warning_flags=%warning_flags% -D_CRT_SECURE_NO_WARNINGS



rem      *******************
rem      * Optimiser Flags *
rem      *******************

set optimiser_flags=

if [%optimise%]==[true] (
  rem enable optimisation
  set optimiser_flags= %optimiser_flags% /O2
) else (
  rem disable optimisation
  set optimiser_flags= %optimiser_flags% /Od
)

rem allow optimisation of floating point operations
set optimiser_flags= %optimiser_flags% /fp:fast

rem generate intrinsics
set optimiser_flags= %optimiser_flags% /Oi



rem      ******************
rem      * Compiler Flags *
rem      ******************

set compiler_flags= %includes% ^
                    %defines% ^
                    %optimiser_flags% ^
                    %warning_flags% ^
                    /nologo

rem full path of source files in diagnostics
set compiler_flags=%compiler_flags% /FC

rem embed debug symbols in .obj
set compiler_flags=%compiler_flags% /Z7

rem better debug when optimised
set compiler_flags=%compiler_flags% /Zo

rem disable minimal rebuild
set compiler_flags=%compiler_flags% /Gm-

rem disable RTTI
set compiler_flags=%compiler_flags% /GR-

rem disable exceptions
set compiler_flags=%compiler_flags% /EHa-



rem      ****************
rem      * Linker Flags *
rem      ****************

set linker_flags= %libraries%

rem disable incremental link
set linker_flags=%linker_flags% /INCREMENTAL:NO

rem remove unreferenced code
set linker_flags=%linker_flags% /OPT:REF

if 0 == 1 (
  rem list libs
  set linker_flags=%linker_flags% /VERBOSE:LIB
)



rem      *********************
rem      * C Runtime Library *
rem      *********************

if 1 == 1 (
  rem Use msvcrt.lib & msvcrXXX.dll
  set compiler_flags=%compiler_flags% /MD
  set linker_flags=%linker_flags% /NODEFAULTLIB:libc.lib /NODEFAULTLIB:libcmt.lib /NODEFAULTLIB:libcd.lib /NODEFAULTLIB:libcmtd.lib /NODEFAULTLIB:msvcrtd.lib
) else (
  rem Use libcmt.lib
  rem FIXME: doesn't work - missing some libs?
  set compiler_flags=%compiler_flags% /MT
  set linker_flags=%linker_flags% /NODEFAULTLIB:libc.lib /NODEFAULTLIB:msvcrt.lib /NODEFAULTLIB:libcd.lib /NODEFAULTLIB:libcmtd.lib /NODEFAULTLIB:msvcrtd.lib
)



rem      *********
rem      * Clean *
rem      *********

if [%clean%]==[true] (
  if exist build rmdir /S /Q build
)



rem      *********
rem      * Build *
rem      *********

if [%build%]==[true] (
  if not exist build mkdir build
  pushd build
  cl %compiler_flags% ^
     %src_files% ^
     /Fmmap.txt ^
     /link %linker_flags%
  popd
)
